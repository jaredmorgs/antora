= Get Help
// URLs
:url-issues: {url-repo}/issues
:url-twitter: https://twitter.com/antoraproject
:url-twitter-hash: https://twitter.com/hashtag/antora?src=hash
:url-contributing: https://gitlab.com/antora/antora/blob/HEAD/contributing.adoc

Antora is designed to help you write and publish your documentation.
However, we can't fully realize this goal without your feedback!
We encourage you to report issues, ask questions, share ideas, or discuss other aspects of this project using the communication tools provided below.

== Issues

The preferred means of communicating problems, ideas, and other feedback is through the project issue tracker.

* {url-issues}[Issue tracker^] (GitLab)

Any significant change or decision about the project is logged there.

== Chat

If you need to switch to real time input, visit our community chat room.

* {url-chat}[Chat^] (Zulip)

Keep in mind that the discussion logs for these rooms are archived, but there's no guarantee those logs will be saved indefinitely.

== Twitter

If you want to share your experience with Antora or help promote it, we encourage you to post about it on social media.
When you talk about Antora on Twitter, you can mention the official account for the project:

* {url-twitter}[@antoraproject^] -- The official Antora account on Twitter.

You can also use the {url-twitter-hash}[#antora^] hashtag to promote the project or discover other people talking about it.

== Code and content contributions

If you're interested in helping the maintainers improve the project, then check out the {url-contributing}[Antora contributing guidelines^].
