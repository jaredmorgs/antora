= Examples Directory and Files

[#examples-dir]
== Examples family directory

A module directory can contain an xref:family-directories.adoc[optional family directory] named [.path]_examples_.

[listing]
----
📂 modules
  📂 a-named-module
    📂 examples <.>
      📄 a-source-file.js <.>
  📂 ROOT
    📂 examples <.>
      📄 a-source-file.csv <.>
      📄 a-source-file.json
    📂 pages
      📄 a-source-file.adoc
    📄 nav.adoc
----
<.> An [.path]_examples_ family directory in a named module directory
<.> An example source file
<.> An [.path]_examples_ family directory in a [.path]_ROOT_ module directory
<.> An example source file

Antora applies preset behavior to the source files stored in an [.path]_examples_ directory when it generates a site.

[#examples]
== Example files

Example files, also called examples, are resources, such as source code files or snippets (e.g., [.path]_runner.js_), terminal output, and data sets (e.g., [.path]_choptank-co2.csv_).
The files are stored in an [.path]_examples_ directory.

Antora doesn't automatically publish example files.
The xref:page:include-an-example.adoc[example must be referenced by an include directive] from a page, or resource that's eventually included in a page, for the example's content to be published.

TIP: Examples shouldn't be confused with the AsciiDoc example block, though an example file can be inserted into an example block using an include directive.
