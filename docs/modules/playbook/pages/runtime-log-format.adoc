= Log Format

The log `format` key specifies the format of the log messages.

IMPORTANT: The log format, and all other log settings, are not honored if a fatal error is thrown before Antora fully configures the playbook.
Instead, the error message is printed directly to standard error (STDERR).

[#default]
== Default log format

Explicitly assigning a value to the `format` key is optional.
When `format` isn't set, Antora assigns one of the key's accepted values, either `json` or `pretty`, based on the environment it detects at runtime.
Antora assigns the value `json` to the `format` key when it detects a <<ci,continuous integration environment (CI=true)>>.
In all other cases, Antora uses the `pretty` value.

[#format-key]
== format key

The `format` key is configured under the xref:configure-runtime.adoc[runtime and log] keys in a playbook.

.antora-playbook.yml
[source,yaml]
----
runtime:
  log:
    format: json
----

The `format` key accepts the following built-in values:

json:: _Default when `CI=true`._
The structured log messages are emitted in JSON format to the standard out stream (STDOUT), one per line, so that they can be piped to other applications and processed.
The messages adhere to the https://jsonlines.org/[JSON Lines] text format, also known as newline-delimited JSON.
The levels of the messages are expressed as labels, _error_, _info_, etc., by default.
The level format can be changed to numbers by setting the <<level-format-key,log.level_format key>> and assigning the value `number` to it.

pretty:: _Default in non-CI environments._
The log messages are formatted for readability and emitted to the standard error stream (STDERR).

The `format` key can also be specified using the <<format-option,--log-format option>> or xref:playbook:environment-variables.adoc#log-format[ANTORA_LOG_FORMAT variable].

.CI environment variable
[#ci]
****
Continuous integration (CI) environments, such as Netlify, GitHub Actions, GitLab CI, and many others, typically set the continuous integration environment variable (`CI`) to `true`.
Antora uses this environment variable to determine when it's running in a CI environment and change its behavior accordingly.
****

[#pretty]
=== Prettified

To emit formatted log messages, assign the `pretty` value to the `format` key in your playbook.

.Assign pretty value to format key
[source#ex-pretty,yaml]
----
runtime:
  log:
    format: pretty
----

When Antora runs, any log messages are emitted to STDERR.
If you run Antora from your terminal, the formatted log messages are displayed there.
<<result-pretty>> shows a prettified log message about an xref error.

.Log message output using pretty format
[listing#result-pretty]
----
[16:03:00.691] ERROR (asciidoctor): target of xref not found: a-page.adoc
    file: /home/computer/my-projects/project/docs/modules/module-name/pages/index.adoc:54 <.>
    source: /home/computer/my-projects/project (refname: my-branch <worktree>, start path: docs)
----
<.> To display the line number where an error occurs, set the xref:asciidoc-sourcemap.adoc[sourcemap key].

[#json]
=== JSON

To emit structured log messages in JSON format, assign the `json` value to the `format` key in your playbook.

.Assign json value to format key
[source#ex-json,yaml]
----
runtime:
  log:
    format: json
----

When Antora runs, any log messages are emitted to STDOUT.
<<result-json>> shows a structured log message about an xref error.

.Log message output in JSON
[listing#result-json]
----
{"level":"error","time":1627682525543,"name":"asciidoctor","file":{"path":"/home/computer/my-projects/project/docs/modules/module-name/pages/index.adoc","line":54},"source":{"url":"https://gitlab.com/org/project.git","worktree":"/home/computer/my-projects/project","refname":"my-branch","startPath":"docs"},"msg":"target of xref not found: a-page.adoc"}
----

A structured log message is made up of a series of key-value pairs.
Each key indicates a log message field, such as _level_, and each value records the logging information for that field, such as _error_.
JSON formatted messages can be sent to separate applications or log ingestion services for parsing, search, and analysis.
<<result-jq>> shows a structured log message about an xref error that's been piped to https://stedolan.github.io/jq/[jq], a command line JSON processor, for pretty printing.

.Log message output piped to jq
[listing#result-jq]
----
{
  "level": "error",
  "time": 1627683497637,
  "name": "asciidoctor",
  "file": {
    "path": "/home/computer/my-projects/project/docs/modules/module-name/pages/index.adoc",
    "line": 54
  },
  "source": {
    "url": "https://gitlab.com/org/project.git",
    "worktree": "/home/computer/my-projects/project",
    "refname": "my-branch",
    "startPath": "docs"
  },
  "msg": "target of xref not found: a-page.adoc"
}
----

[#format-option]
== Log format option

You don't have to modify the playbook file directly to set the `format` key.
You can use the `--log-format` option from the xref:cli:options.adoc#log-format[CLI].

 $ antora --log-format=json antora-playbook.yml

The `--log-format` option overrides the value assigned to the `format` key or to the xref:playbook:environment-variables.adoc#log-format[ANTORA_LOG_FORMAT environment variable].

[#level-format-key]
== level_format key

When the log format is JSON (`json`), each log level correlates to a label and a number.
The JSON format expresses a level as a label, such as _error_ or _info_, by default.
However, some tools require the level to be a number.
The format of the level can be configured with the `level_format` key.
The `level_format` key is configured under the xref:configure-runtime.adoc[runtime and log] keys in a playbook.

.Assign number value to level_format key
[source#ex-number,yaml]
----
runtime:
  log:
    format: json
    level_format: number
----

The `level_format` key accepts the built-in values `label` and `number`.
The default value is `label`.
If the log format is `pretty`, the value assigned to the `level_format` key is ignored and levels are always expressed as labels.

[#level-format-option]
== Level format option

You don't have to modify the playbook file directly to set the `level_format` key.
You can use the `--log-level-format` option from the xref:cli:options.adoc#log-level-format[CLI].

 $ antora --log-format=json --log-level-format=number antora-playbook.yml

The `--log-level-format` option overrides the value assigned to the `level_format` key or to the xref:playbook:environment-variables.adoc#log-level-format[ANTORA_LOG_LEVEL_FORMAT environment variable].
