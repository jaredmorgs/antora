= Examples
:page-aliases: examples-and-source-snippets.adoc

== Reusable, single source examples

Examples are useful for storing source code, queries, configuration parameters, terminal output, logs, data sets, and other non-AsciiDoc files, that you reuse in one or more pages throughout your site.
The content of example files is usually inserted into source, listing, and literal blocks.
Example files are stored in a xref:ROOT:examples-directory.adoc[_examples_ directory] and, regardless of which component version they belong to, can be referenced by any page or partial in your site.
An example is referenced using its resource ID and the xref:include-an-example.adoc[AsciiDoc include directive].
Select regions or lines from an example, instead of all of the content in the example file, can be inserted into a page using the include directive's `tag`, `tags`, or `lines` attributes.
Changes you make to the example will disseminate to all of the pages where you referenced it the next time you build your site.

TIP: Examples shouldn't be confused with the AsciiDoc example block, though they can be inserted into an example block using an include directive.

//== Source materials used in other applications
