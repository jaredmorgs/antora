= Include a Partial
:family: partial
:coordinate: partial$
:ext: .adoc
:page-aliases: asciidoc:include-partial.adoc
// URLs
:url-tags: {url-asciidoc}/directives/include-tagged-regions/
:url-leveloffset: {url-asciidoc}/directives/include-with-leveloffset/

A partial can be inserted into any page or another partial in your site using the AsciiDoc include directive and the partial's resource ID.

== Insert a partial into a page

include::partial$include-directive.adoc[]

=== Include directive placement

[listing]
----
A paragraph in the standard page.

include::partial$cli-options.adoc[] <.>

Another paragraph in the standard page.
include::partial$addendum.adoc[] <.>
----
<.> Enter the include on a new line.
If you want the partial contents to be a standalone block, make sure there's a blank line above and below it.
<.> To attach the partial to another block, enter it on a new line but don't insert a blank line between the include and the block content.

== Include a partial from another module

When the target partial and the current page belong to different modules of the same component version, enter the partial's module, family, and file coordinates in the resource ID.

.current-page.adoc
----
\include::ROOT:partial$defs.adoc[]
----

== Include a partial from another component

When the target partial and the current page belong to different docs components, enter the partial's version, component, module, family, and file coordinates in the resource ID.

.current-page.adoc
----
\include::2.0@catalog::partial$defs.adoc[]
----

Don't specify the version coordinate if you want a page to always reference the latest version of the partial.

.current-page.adoc
----
\include::catalog::partial$defs.adoc[]
----

//== Include a tagged region from a partial

== Learn more

* xref:include-an-example.adoc[Include an example]
* xref:include-a-page.adoc[Include a standard page]
* xref:resource-id.adoc[Resource IDs]

.*AsciiDoc and Asciidoctor resources*
* {url-leveloffset}[Offset section headings with leveloffset^]
* {url-tags}[Select regions of content with tags^]
