= Resource ID Coordinates

[#id-coordinates]
== Coordinate syntax and order

A xref:resource-id.adoc#whats-a-resource-id[resource ID] contains five coordinates: version, component, module, family, and file.
Antora constructs a resource ID for a source file by assigning values to these coordinates based on the file, xref:ROOT:standard-directories.adoc[standard directory set], and information in the xref:ROOT:component-version-descriptor.adoc[component version descriptor].
You can reference a resource from any other resource--regardless of the component version each resource belongs to--using a sequence of the resource ID coordinates assigned to a resource.

<<fig-full-id>> shows each coordinate type and their sequence in a fully qualified resource ID.

.Fully qualified resource ID
[#fig-full-id]
image::full-resource-id.svg[Diagram of a fully qualified Antora resource ID]

[[full-id]]When all of the coordinates in a resource ID are specified, it's referred to as a [.term]*fully qualified resource ID*.
You probably won't use a fully qualified resource ID when referencing a resource that often.
How many resource ID coordinates you specify when referencing a resource depends on the component version and module of the current page in relation to the target resource, the family of the target resource, and the AsciiDoc syntax used to reference the target resource.
[[target]]The [.term]*target resource* is the resource source file that's referenced, by specifying its resource ID, as the value in an AsciiDoc macro, include directive, or other syntax.
[[current]]The [.term]*current page* is the page file that contains the AsciiDoc syntax that references the target resource.

.A page containing an xref that references a resource
[#fig-target]
image::target-resource.svg[Example of an xref in the current page referencing a target resource]

In <<fig-target>>, the page titled _Satellites_ is the current page and the resource with the filename _modes.adoc_ is the target resource being referenced by the xref macro.
//The page titled _Satellites_ contains an xref macro that specifies the resource ID of the target resource.
//Because the specified resource ID uses the version and component coordinates, we can surmise that the current page and the target resource belong to different component versions.
The following sections describe how to determine the value of each coordinate for a target resource and when to use each coordinate.

[#id-version]
=== Version coordinate

The version coordinate is the same value as the version of the component to which the <<target,target resource>> belongs.
The value of the version is assigned to the xref:ROOT:component-version-key.adoc[version key].
The `version` key is specified either in the [.path]_antora.yml_ file that defines the resource's component version or on the content source in your playbook from where Antora collected the component version's files.

The version coordinate should be used when the target resource and current page belong to difference component versions.
When entering a version coordinate in a resource ID, it should always be the first coordinate and end with an at sign (`@`).

[#id-component]
=== Component coordinate

The component coordinate is the name of the component to which the target resource belongs.
The component name is specified by the xref:ROOT:component-name-key.adoc[name key] in the [.path]_antora.yml_ file that defines the resource's component version.

The component coordinate must be used when the target resource and current page belong to different documentation components.
The component name is entered before the module coordinate in a resource ID and is directly followed by one colon (`:`).

[#id-module]
=== Module coordinate

The module coordinate is the name of the module to which the target resource belongs.
The module name is derived from the xref:ROOT:module-directories.adoc#module[module directory] where the resource is stored.

The module coordinate must be used when the target resource and current page belong to different documentation components or different modules within the same component.
The module name is entered before the family coordinate, if the family coordinate is present, or before the file coordinate if the family coordinate isn't present.
The module name is directly followed by one colon (`:`).

[#id-family]
=== Family coordinate

The family coordinate identifies the name of the family to which the target resource belongs.
The family is derived from the xref:ROOT:family-directories.adoc[family directory] where the resource is stored.
Remove the "`s`" at the end of the family's name and replace it with a dollar sign (`$`) when constructing a resource ID.
The valid family coordinates are `page$`, `image$`, `partial$`, `example$`, and `attachment$`.

The family coordinate doesn't always need to be specified in a resource ID.
Whether the family coordinate needs to be entered depends on the family of the target resource and the AsciiDoc syntax used to reference the target resource.
For example, if you reference a page using the xref macro, you don't need to use the `page$` family coordinate because the xref macro applies it by default when no family coordinate is specified in the entered resource ID.
The following table lists when the family coordinate is required depending on the resource being referenced and the syntax that is referencing the resource.

[#reference-syntax-id-requirements]
[cols="2,2,4"]
|===
|Resource being referenced |Syntax referencing the resource |Family coordinate required?

|xref:attachments.adoc[Attachment]
|Xref macro
|*Yes, the `attachment$` coordinate is required*.
See xref:attachments.adoc[].

|xref:examples.adoc[Example]
|Include directive
|*Yes, the `example$` coordinate is required*.
See xref:include-an-example.adoc[].

.3+|xref:images.adoc[Image]
|Block image macro
|No, the family coordinate isn't required.
See xref:block-images.adoc[] and xref:image-resource-id-examples.adoc[].

|Inline image macro
|No, the family coordinate isn't required.
See xref:inline-images.adoc[] and xref:image-resource-id-examples.adoc[].

|Xref macro
|*Yes, the `image$` coordinate is required*.

.2+|xref:index.adoc[Page]
|Xref macro
|No, the family coordinate isn't required.

|Include directive
|No, the family coordinate isn't required.
See xref:include-a-page.adoc[].

|xref:partials.adoc[Partial]
|Include directive
|*Yes, the `partial$` coordinate is required*.
See xref:include-a-partial.adoc[].
|===

[#id-resource]
=== File coordinate

The file coordinate specifies the path, relative to the xref:ROOT:family-directories.adoc[family directory], of the target resource's source file.
The file coordinate must include the file extension of the target resource except in special circumstances when the resource doesn't have a file extension, such as _Dockerfile_, and it's also classified as a partial or example.
If both the target resource and the current page are located in the same subdirectory of a family directory, the path relative to the family directory must be entered as part of the file coordinate.
You can add `./` to the start of the file coordinate as a shorthand for the family-relative path of the target resource.

//[#requires-family-coordinate]
//== When is the family coordinate required?
