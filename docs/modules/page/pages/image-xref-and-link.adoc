= Resource Links From Images

You can reference an element, page, attachment, or image from an image using the `xref` attribute in an inline or block image macro.

== Xref attribute

The `xref` element attribute can be set in the attribute list of block and inline image macros.
The attribute accepts the resource ID of a publishable resource (page, attachment, or image) or the xref:xref.adoc#id-fragment[ID of an element (fragment)] in the current page.

.Reference a publishable resource from an image macro
[source#ex-base]
----
image::my-image.jpg[xref=version@component:module:family$file.ext]
----

.Reference an element in the current page from an image macro
[source#ex-id]
----
image::my-image.jpg[xref=#fragment] <.>
----
<.> To reference an element in the current page, prefix the element's ID with the hash symbol (`#`).

When your site is published and a visitor clicks on the embedded image, they'll be linked to the URL of the published resource or element in the published version of the current page.

== Reference a page from an image

The following examples assume that the target resource assigned to the `xref` attribute is a page, and that the target page and xref:xref.adoc#current[current page] belong to the same component version and module.
Therefore, the resource ID of the target page only requires the file coordinate.

The block image macro shown in <<ex-page>> will embed the image _peak.svg_ and create a link from the image to the published URL of the page _modes.adoc_.

.current-page.adoc (block)
[source#ex-page]
----
image::peak.svg[xref=modes.adoc]
----

The `xref` attribute behaves the same way when set and assigned in an inline image macro.

.current-page.adoc (inline)
[listing#ex-inline-page]
----
image:peak.svg[xref=modes.adoc]
----
